import React, { useState, useEffect } from 'react'
import Crystal from './Crystal'
import config from 'visual-config-exposer'
export default function() {
    const ItemArray=config.settings.items
    var length=Object.keys(ItemArray).length-1
    /*var randNum = Math.floor(Math.random() * (5-0+1)) + 0;*/
    var [state, setState] = useState(length)
    var [count, setCount]=useState(1)
    var [isDrawing, setIsDrawing] = useState(false)
    const RewardImage=Object.entries(ItemArray)[state][1].image
    const label=Object.entries(ItemArray)[state][1].label
    const probabilit=Object.entries(ItemArray)[state][1].probability
    const colorHeading =config.settings.titleColor;
    const colorDesc=config.settings.desColor
    const labelColor=config.settings.labelColor
    const font = config.settings.titleFont;
    var itemProb=[]
    var add=0;
    for(let i=0; i<length+1;i++){
    const prob=Object.values(ItemArray)[i].probability
    add=add+parseFloat(prob)
    itemProb.push(prob)
    
    }

 const randNum = Math.floor(Math.random() * (5-0+1)) + 0;
 var selectedProb=itemProb[randNum]

    useEffect((e) =>{
      if(count>1){
          return;
      }
       
    var element = document.createElement("link");
    /*var num = Math.random().toFixed(2)*/
    var background=config.settings.backgroundImage
    console.log(background)
    document.body.style.backgroundImage="url("+background+")"
    element.setAttribute("rel", "stylesheet");
    element.setAttribute("type", "text/css");
    element.setAttribute("href", "https://fonts.googleapis.com/css?family="+font);
    document.getElementsByTagName("head")[0].appendChild(element)
       
        document.getElementsByTagName('h1')[0].style.color=colorHeading;
        document.getElementsByTagName('h1')[0].style.fontFamily=font;
        document.getElementsByTagName('h3')[0].style.fontFamily=font;
        document.getElementsByTagName('h3')[0].style.color=colorDesc;
        document.getElementsByTagName('h2')[0].style.fontFamily=font;
        document.getElementsByTagName('h2')[0].style.color=labelColor;

        const ball=document.getElementById("ball")
        ball.addEventListener("mousedown", startScratch)
        ball.addEventListener("mousemove", scratch)
        ball.addEventListener('mouseup',stopScratch)

        ball.addEventListener('touchstart', startScratch)
        ball.addEventListener('touchmove', scratch)
        ball.addEventListener('touchend', stopScratch)
   
 
        function startScratch(e){
            setIsDrawing(true)
        }
    
        function scratch(e) {
            setTimeout(function () {
                setCount(count+1)
                if(!isDrawing) {
                    return;
                    } 
                   
                    document.getElementsByTagName('button')[0].style.visibility="visible"
                   document.getElementsByTagName('h3')[0].style.visibility='hidden'
                   document.getElementsByTagName('h2')[0].style.visibility='visible'
                   document.getElementById('imageball').style.visibility="visible"
                  
                   document.getElementById('mainball').style.visibility="hidden"
                   document.getElementsByClassName('hide')[0].style.visibility='visible'
                   document.getElementsByClassName('mainball')[0].style.pointerEvents='none'   
                   
            },1000)
           
        }
    
        function stopScratch() {
            setIsDrawing(false)
            setCount(count+1)
        }
    
        

    if((selectedProb>0) && (selectedProb<0.1)) {
       /* setState(length-(length-1))*/
       setState(randNum)
       
      
    }
    if((selectedProb>0.1) && (selectedProb<0.2)) {
        /*setState(length-(length-2))*/
        setState(randNum)
      
        
    }
    if((selectedProb>0.2) && (selectedProb<0.3)) {
       /* setState(length-(length-3))*/
        setState(randNum)
        
        
    }
    if((selectedProb>0.3) && (selectedProb<0.4)) {
       /* setState(length-(length-4))*/
        setState(randNum)
        
      
    }
    if((selectedProb>0.4) && (selectedProb<0.5)) {
        /* setState(length-(length-4))*/
         setState(randNum)
         
         
     }
     if((selectedProb>0.5) && (selectedProb<0.6)) {
        /* setState(length-(length-4))*/
         setState(randNum)
      
        
     }
     if((selectedProb>0.6) && (selectedProb<0.7)) {
        /* setState(length-(length-4))*/
         setState(randNum)
         
        
     }
     if((selectedProb>0.7) && (selectedProb<0.8)) {
        /* setState(length-(length-4))*/
         setState(randNum)
         
         
     }
     if((selectedProb>0.8) && (selectedProb<0.9)) {
        /* setState(length-(length-4))*/
         setState(randNum)
      
     }
     if((selectedProb>0.) && (selectedProb<=1)) {
        /* setState(length-(length-4))*/
         setState(randNum)
        
        
     }
if(add>1) {
    console.log('Sum of Probabilty is more then one')
}

})

    
   

function refresh() {
           window.location.reload(true)
           
           document.getElementsByTagName('button')[0].style.visibility="hidden"
           document.getElementsByClassName('hide')[0].style.visibility="hidden"
           
           
}
    

    return(
        
 <>
    <div className='card'>
    <h1>{config.settings.title}</h1>
    <h3>{config.settings.description}</h3>
    <h2>{label}</h2>
    <button onClick={refresh}><i className="fas fa-redo-alt"></i></button>
    <div className='ball'>
    <Crystal RewardImage={RewardImage}/>
    </div>
    
</div>
</>
    )
}
